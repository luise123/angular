import { v4 as uuid } from 'uuid';
export class DestinoViaje {

    public selected: boolean;
    public servicios: string[];
    public id = uuid();
    //public votes = 0;
    constructor(public nombre: String, public imagenUrl: string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
    }
    public isSelected(): boolean {
        return this.selected;
    }

    public setSelected(s: boolean) {
        this.selected = s;
    }

    voteUp(): any {
        this.votes++;
    }
    voteDown(): any {
        this.votes--;
    }
}
