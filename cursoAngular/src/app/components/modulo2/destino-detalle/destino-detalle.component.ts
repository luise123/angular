import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinoApiClient } from 'src/app/models/destinos-api-client.model';

/*class DestinosApiClientViejo {
  getById(id:String):DestinoViaje{
    console.log('llmando por la clase vieja');
    return null;
  }
}

interface AppConfig{
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
}

const APP_CONFIG = new InjectionToken<AppConfig>('app.config')

@Injectable()
class DestinosApiClientDecorated extends DestinoApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
    super(store);
  }

  getById(id:String):DestinoViaje{
    console.log('llamando por la clase decorada!!!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);

  }
}*/
//https://stackblitz.com/edit/ngx-mapbox-gl-without-token?file=app%2Fapp.component.ts
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    DestinoApiClient
    /*DestinoApiClient,
    {provide: DestinosApiClientViejo, useExisting: DestinoApiClient}*/
    /*{provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {provide: DestinoApiClient, useClass:DestinosApiClientDecorated},//la clase de useClass debe ser heredada de la que esta en provide
    {provide: DestinosApiClientViejo, useExisting: DestinoApiClient}*/
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinoApiClient) { }

  ngOnInit() {
    console.log("destino-detalle.ngOnInit")

    const id = this.route.snapshot.paramMap.get('id');
    console.log(id)
    this.destino = this.destinosApiClient.getById(id);
    console.log(this.destino)

  }

}
