import { state } from '@angular/animations';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { DestinoApiClient } from 'src/app/models/destinos-api-client.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/models/destinos-viajes-state.model';
import { DestinoViaje } from '../../../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [DestinoApiClient]
})

export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  public list:DestinoViaje[];

  constructor(public destinoApiClient:DestinoApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    //this.destinos = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push("Se ha elegido a " + d.nombre);
        }
      });

      this.store.select(state => state.destinos.items).subscribe( d =>{
        if (d != null) {
          if(d.length > 0){
            this.all = [];
            console.log(d)
            d.forEach( x => {
              var des = new DestinoViaje(x.nombre, x.imagenUrl,x.votes);
              des.id = x.id;
              des.selected = x.selected
              this.all.push(des);
            });
          }
        }
      });

      ///this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  };

  agregado(d: DestinoViaje){
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  ngOnInit(): void {
  }

  elegido(e:DestinoViaje){
    this.destinoApiClient.elegir(e);
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }
}
