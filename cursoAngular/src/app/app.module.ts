import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store} from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects';
import {HttpClient, HttpClientModule, HttpHeaders, HttpRequest} from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import Dexie from 'dexie';//db
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  DestinosViajesState,
  intializeDestinosViajesState,
  reducerDestinosViajes,
  DestinosViajesEffects,
  InitMyDataAction
} from './models/destinos-viajes-state.model';
import { AppComponent } from './app.component';
import { Modulo1Component } from './components/modulo1/modulo1.component';
import { ListaDestinoComponent } from './components/modulo1/lista-destino/lista-destino.component';
import { DestinoViajeComponent } from './components/modulo1/destino-viaje/destino-viaje.component';
import { DestinoDetalleComponent } from './components/modulo2/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/modulo2/form-destino-viaje/form-destino-viaje.component';
/*import { DestinoApiClient } from './models/destinos-api-client.model';*/
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';


//app config
export interface AppConfig{
  apiEnpoint: String
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEnpoint: 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>{
  return () => appLoadService.initializeDestinosViajesState();
}
@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState(): Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEnpoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin app init

//dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie{
  destinos: Dexie.Table<DestinoViaje,number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos:'++id, nombre, imagenUrl'
    });
    this.version(2).stores({
      destinos:'++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();
//fin dexie db

// i18n ini
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){ }

  getTranslation(lang: string):Observable<any>{
    const promise = db.translations
                        .where('lang')
                        .equals(lang)
                        .toArray()
                        .then(results => {
                          if(results.length === 0){
                            return this.http
                            .get<Translation[]>(APP_CONFIG_VALUE.apiEnpoint + '/api/translation?lang=' + lang)
                            .toPromise()
                            .then(apiResults => {
                              db.translations.bulkAdd(apiResults);
                              return apiResults;
                            });
                          }
                          return results;
                        }).then((traducciones) => {
                          console.log('traducciones cargadas:');
                          console.log(traducciones);
                          return traducciones;
                        }).then((traducciones) => {
                          return traducciones.map((t) => ({[t.key]: t.value}));
                        });
            return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}
// fini18n


export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component:  VuelosDetalleComponentComponent },
];

const routes: Routes = [
  {path: '', redirectTo:'home', pathMatch: 'full'},
  {path:'home', component:ListaDestinoComponent},
  {path:'destino/:id', component:DestinoDetalleComponent},
  {path:'login', component:LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },{
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }
];


//redux init
export interface AppState {
  destinos: DestinosViajesState;
};

let reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
    destinos: intializeDestinosViajesState()
};
//fin redux init

@NgModule({
  declarations: [
    AppComponent,
    Modulo1Component,
    DestinoViajeComponent,
    ListaDestinoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgxMapboxGLModule,
    BrowserAnimationsModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),//redux
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),

  ],
  providers: [AuthService, UsuarioLogueadoGuard, //DestinoApiClient
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService, {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi:true},
    MyDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
